From maven:3-jdk-8 as mvn
RUN git clone https://github.com/spring-projects/spring-petclinic.git
RUN cd spring-petclinic && mvn package

FROM alpine:latest
RUN apk --no-cache add openjdk11 --repository=http://dl-cdn.alpinelinux.org/alpine/edge/community
LABEL AUTHOR="DEVOPS"
COPY --from=mvn /spring-petclinic/target/spring-petclinic*.jar /spring-petclinic.jar
EXPOSE 8080
CMD ["java", "-jar","/spring-petclinic.jar"]

COPY ./script.sh /script.sh
RUN chmod 0755 /script.sh
RUN touch /var/log/cron.log
RUN touch crontab.tmp \
    && echo '* * * * * sh /script.sh >> /var/log/cron.log 2>&1' > crontab.tmp \
    && crontab crontab.tmp \
    && rm -rf crontab.tmp

CMD ["/usr/sbin/crond", "-f", "-d", "0"]

