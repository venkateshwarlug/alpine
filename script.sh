#!/bin/sh
SERVICE=jar
if ps ax | grep -v grep | grep $SERVICE > /dev/null
then
    echo "The Jar service is already running. If you want to restart, please kill or stop the running Jar first."
else
    echo "Starting the Jar. "
java -jar /spring-petclinic.jar &
fi

